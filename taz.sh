#!/bin/bash

# taz 0.2 by mativ
#

# Check if mpg123 is installed
if ! command -v mpg123 &> /dev/null; then
    echo "Error: mpg123 is not installed. Please install mpg123 to use this script."
    exit 1
fi

# Create an array to store all the file addresses
addresses=()

# Check if the help option is passed
if [[ $1 == "-h" || $1 == "--help" ]]; then
    echo "Usage: $(basename "$0") [directory1] [directory2] [music_file1] [music_file2] [mpg123_options] ..."
    echo "If no arguments are provided, the current directory will be played."
    echo "Additional options will be passed directly to mpg123."
    exit 0
fi

# Check if only the -q option is provided
if [[ $1 == "-q" ]]; then
    # Find music files in the current directory
    while IFS= read -r -d '' file; do
        addresses+=("$file")
    done < <(find . -type f \( -name "*.mp3" -o -name "*.mp2" -o -name "*.mp1" -o -name "*.ogg" -o -name "*.wav" -o -name "*.flac" \) -print0 2>/dev/null)

    # Use mpg123 to play the files in the current directory, passing the -q option
    mpg123 -q "${addresses[@]}"
    exit 0
fi

# Create arrays to store directories and mpg123 options
directories=()
mpg123_options=()

# Check if any arguments are provided
if [ $# -eq 0 ]; then
    directories+=(".")
else
    # Iterate over the command-line arguments
    for arg in "$@"; do
        # Check if the argument starts with a dash (-), indicating an option for mpg123
        if [[ $arg == -* ]]; then
            # Pass the option directly to mpg123
            mpg123_options+=("$arg")
        else
            # Check if the argument is a directory
            if [ -d "$arg" ]; then
                directories+=("$arg")
            else
                # Check if the argument is a supported music file
                if [[ "$arg" == *.mp3 || "$arg" == *.mp2 || "$arg" == *.mp1 || "$arg" == *.ogg || "$arg" == *.wav || "$arg" == *.flac ]]; then
                    addresses+=("$arg")
                fi
            fi
        fi
    done
fi

# Find all supported music files in the directories and append them to the addresses array
for dir in "${directories[@]}"; do
    while IFS= read -r -d '' file; do
        addresses+=("$file")
    done < <(find "$dir" -type f \( -name "*.mp3" -o -name "*.mp2" -o -name "*.mp1" -o -name "*.ogg" -o -name "*.wav" -o -name "*.flac" \) -print0 2>/dev/null)
done

# Check if no valid files were found and no additional options were provided
if [ ${#addresses[@]} -eq 0 ] && [ ${#mpg123_options[@]} -eq 0 ]; then
    echo "No valid music files found."
    exit 1
fi

mpg123 "${mpg123_options[@]}" "${addresses[@]}"