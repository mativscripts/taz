# Taz

Taz is a simple bash script to make playlists of your mp3 files and play them using the mpg123 player.

### Dependencies

- Bash (version 4 or above)
- mpg123 player

### Installation

(copying to usr/local/bin is optional, but useful to get access without referring to file everytime)

   ```shell
   git clone https://gitlab.com/mativscripts/taz.git
   cd taz
   chmod +x taz.sh
   sudo cp taz.sh /usr/local/bin/taz
   taz -h
   ```


License: GNU GPLv3
